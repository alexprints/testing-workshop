# Testing Workshop

## Getting Started

1. Clone this repo and `cd` into the the project folder.
1. Install the dependencies by running `npm install`
1. Verify everything works by running `npm test` . You should see something like this in your terminal:
   - ![](https://i.imgur.com/dqFPclj.png)

## Credits

This workshop is heavily influenced by the String Calculator Kata by Roy Osherove.

https://osherove.com/tdd-kata-1

## Workshop

We are going to practice using tests as not just a measure of correctness, but as a feedback loop for development. In this workshop you are going to implement a basic string parsing program and the requirements are going to be revealed to you step by step so you are not tempted to jump ahead.

During this exercise you should adopt a test first mentality (aka Test Driven Development aka TDD). Follow this flow:

**RED** - Write a failing test first. Ensure your test fails in away that you expect.

**GREEN** - Write just enough code to make your test pass. Try to focus on writing code that satifies your test and nothing more.

**REFACTOR** - Before you move onto adding more functionality, consider if you want to refactor your code. While you are "green" is the perfect time to clean up a nasty loops, excess state and clean up your variable names.

**REPEAT** - Pick the next feature/functionality you want to add and write/modify a failing test.

## Implement the following

<details>
   <summary>Step 1</summary>

The functiona can take up to two numbers, separated by commas, and will return their sum.

Example:
`1,1`

</details>

<details>
   <summary>Step 2</summary>

The function should handle an unknown amount of numbers seperated by commas.

Example:
`1,1,2,3`

</details>

<details>
   <summary>Step 3</summary>

The function should handle commas and new line characters `\n` as delimiters.

Example:
`1\n1,2,3`

</details>

<details>
   <summary>Step 4</summary>

Support declaring custom delimiters using the followin syntax.

Example:
`//[delimiter]\n[numbers…]` or `//;\n1;2;3`

</details>

<details>
   <summary>Step 5</summary>

Support throwing an exception if a negative number is passed in

Example:
`1,-2,3`

</details>
