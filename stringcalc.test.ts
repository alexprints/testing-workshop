import { string_calculator } from "./stringcalc";

describe("String Calculator", () => {

    it("should return 0 for empty string", () => {
        const result = string_calculator("");
        expect(result).toBe(0);
    })
});